﻿using Innergy.Services;
using System;
using System.Linq;

namespace Innergy.Logic
{
    public class ImportWarehouseDataService : IImportWarehouseDataService
    {
        private readonly IStandardInputServcie standardInputServcie;
        private readonly IWarehouseService warehouseService;
        private readonly IWarehouseOutputService warehouseOutputService;
        public ImportWarehouseDataService(IStandardInputServcie standardInputServcie, IWarehouseService warehouseService, IWarehouseOutputService warehouseOutputService)
        {
            this.standardInputServcie = standardInputServcie;
            this.warehouseService = warehouseService;
            this.warehouseOutputService = warehouseOutputService;
        }
        public void ImportWarehouse()
        {
            var skipConditionForIncorrectLines = CreateSkipCondition();
            var warehouseRawData = standardInputServcie.ReadText(true, skipConditionForIncorrectLines);
            var warehouseData = warehouseService.ExtractWarehouseData(warehouseRawData.ToList());
            warehouseOutputService.DisplayWarehouseData(warehouseData);
        }

        private Func<string, bool> CreateSkipCondition()
        {
            return (line) => string.IsNullOrWhiteSpace(line) || line[0] == '#';
        }
    }
}
