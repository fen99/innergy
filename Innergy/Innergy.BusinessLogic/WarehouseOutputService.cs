﻿using Innergy.Domain;
using Innergy.Services;
using System.Collections.Generic;
using System.Linq;

namespace Innergy.Logic
{
    public class WarehouseOutputService: IWarehouseOutputService
    {
        private IStandardOutputService standardOutputService;
        public WarehouseOutputService(IStandardOutputService standardOutputService)
        {
            this.standardOutputService = standardOutputService;
        }
        public void DisplayWarehouseData(IList<Warehouse>  warehouseData)
        {
            foreach (var warehouse in warehouseData)
            {
                standardOutputService.WriteLine();
                standardOutputService.WriteLine($"{warehouse.Name}  (total {warehouse.TotalMaterials})");
                foreach (var materialInfo in warehouse.MaterialsInfos)
                {
                    standardOutputService.WriteLine($"{materialInfo.Id}  : {materialInfo.Quantity}");
                }
                standardOutputService.WriteLine();
            }
        }
    }
}
