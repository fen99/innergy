﻿using Innergy.Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Innergy.Logic
{
    public class WarehouseService : IWarehouseService
    {
       private readonly IMaterialInfoExtractService materialInfoExtractService;
       private readonly IWarehouseInfoExtractService warehouseInfoExtractService;

        public WarehouseService(IMaterialInfoExtractService materialInfoExtractService, IWarehouseInfoExtractService warehouseInfoExtractService)
        {
            this.materialInfoExtractService = materialInfoExtractService;
            this.warehouseInfoExtractService = warehouseInfoExtractService;
        }

        public IList<Warehouse> ExtractWarehouseData(IList<string> warehouseSource)
        {
            var warehouseData = new List<Warehouse>();
            foreach (var rawSource in warehouseSource)
            {
                string[] materialWarehouseRawInfos = GetRawInfo(rawSource);
            
                foreach (var rawWarehouseInfo in materialWarehouseRawInfos)
                {
                    var material = materialInfoExtractService.ExtractMaterialInfoData(rawSource, ';');
                    material.Quantity = materialInfoExtractService.ExtractQuantity(rawWarehouseInfo, ',', 1);
                    ProcessWarehouse(warehouseData, material, rawWarehouseInfo);
                }
            }

            var result = CalculateTotalMaterialsQuantity(warehouseData);
            SortMaterials(warehouseData);
            result = result.OrderByDescending(r => r.Name).OrderByDescending(r => r.TotalMaterials).ToList();
            return result;
        }

        private void SortMaterials(IList<Warehouse> warehouseData)
        {
            foreach (var warehouse in warehouseData)
            {
                warehouse.MaterialsInfos = warehouse.MaterialsInfos.OrderBy(m => m.Id).ToList();
            }
        }

        private IList<Warehouse> CalculateTotalMaterialsQuantity(IList<Warehouse> warehouseData)
        {
            foreach (var warehouse in warehouseData)
            {
                warehouse.TotalMaterials = warehouse.MaterialsInfos.Sum(m => m.Quantity);
            }

            return warehouseData;
        }

        private void ProcessWarehouse(List<Warehouse> warehouseData, MaterialInfo material, string rawWarehouseInfo)
        {
            var warehouseName = warehouseInfoExtractService.GetWarehouseName(rawWarehouseInfo, ',', 0);
            if(warehouseName!=null)
            {
                var warehouse = warehouseData.SingleOrDefault(w => w.Name == warehouseName);

                if (warehouse != null)
                {
                    warehouse.MaterialsInfos.Add(material);
                    return;
                }

                AddNewWarehouse(warehouseData, material, warehouseName);
            }
        }

        private static void AddNewWarehouse(List<Warehouse> warehouseData, MaterialInfo material, string warehouseName)
        {
            var warehouse = new Warehouse
            {
                MaterialsInfos = new List<MaterialInfo>(),
                Name = warehouseName
            };
            warehouse.MaterialsInfos.Add(material);
            warehouseData.Add(warehouse);
        }

        private  string[] GetRawInfo(string rawSource)
        {
            var materialRawInfo = rawSource.Split(';');
            var materialWarehouseRawInfos = materialRawInfo[2].Split('|');
            return materialWarehouseRawInfos;
        }
    }
}
