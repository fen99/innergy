﻿namespace Innergy.Logic
{
    public class WarehouseInfoExtractService: IWarehouseInfoExtractService
    {
        public string GetWarehouseName( string rawWarehouseInfo, char seperator, int warehouseNameIndex)
        {
           var warehouseRaw = rawWarehouseInfo.Split(seperator);
            return warehouseRaw[warehouseNameIndex];
        }
    }
}
