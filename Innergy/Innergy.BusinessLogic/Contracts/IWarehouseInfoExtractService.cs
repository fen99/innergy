﻿namespace Innergy.Logic
{
    public interface IWarehouseInfoExtractService
    {
        string GetWarehouseName(string rawWarehouseInfo, char seperator, int warehouseNameIndex);
    }
}