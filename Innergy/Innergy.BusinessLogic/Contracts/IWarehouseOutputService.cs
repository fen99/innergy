﻿using Innergy.Domain;
using System.Collections.Generic;

namespace Innergy.Logic
{
    public interface IWarehouseOutputService
    {
        void DisplayWarehouseData(IList<Warehouse> warehouseData);
    }
}