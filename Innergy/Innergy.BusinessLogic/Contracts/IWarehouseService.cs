﻿using Innergy.Domain;
using System.Collections.Generic;

namespace Innergy.Logic
{
    public interface IWarehouseService
    {
        IList<Warehouse> ExtractWarehouseData(IList<string> warehouseSource);
    }
}
