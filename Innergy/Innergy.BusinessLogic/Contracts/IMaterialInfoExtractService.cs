﻿using Innergy.Domain;

namespace Innergy.Logic
{
    public interface IMaterialInfoExtractService
    {
        int ExtractQuantity(string dataLine, char seperator, int indexOfDataElement);
        MaterialInfo ExtractMaterialInfoData(string dataLine, char seperator);
    }
}