﻿using Innergy.Domain;

namespace Innergy.Logic
{
    public class MaterialInfoExtractService: IMaterialInfoExtractService
    {
   
        public MaterialInfo ExtractMaterialInfoData(string dataLine, char seperator)
        {
            var materialsInfo = dataLine.Split(seperator);
            var materialName = materialsInfo[0];
            var materialId = materialsInfo[1];

            return new MaterialInfo
            {
                Id = materialId,
                Name = materialName
            };
        }

        public int ExtractQuantity(string dataLine, char seperator, int indexOfDataElement)
        {
            var materialsInfo = dataLine.Split(seperator);
            var quantity = int.Parse(materialsInfo[indexOfDataElement]);
            return quantity;
        }

    }
}
