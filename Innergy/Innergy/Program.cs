﻿using Innergy.Logic;
using Innergy.Services;

namespace Innergy
{
    class Program
    {
        static void Main(string[] args)
        {
            IImportWarehouseDataService warehouseService = WarehouseCompositionRoot.Create();
            warehouseService.ImportWarehouse();
        }
    }
}