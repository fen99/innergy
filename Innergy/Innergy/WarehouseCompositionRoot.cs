﻿using Innergy.Logic;
using Innergy.Services;

namespace Innergy
{
    internal static class WarehouseCompositionRoot
    {
        public static ImportWarehouseDataService Create()
        {
            return new ImportWarehouseDataService(new StandardInputServcie(new InputService()), new WarehouseService(new MaterialInfoExtractService(), new WarehouseInfoExtractService()), new WarehouseOutputService(new StandardOutputService()));
        }
    }
}
