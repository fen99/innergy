﻿namespace Innergy.Services
{
    public interface IStandardOutputService
    {
        void WriteLine();
        void WriteLine(string message);
    }
}