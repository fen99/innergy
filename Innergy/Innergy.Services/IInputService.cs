﻿namespace Innergy.Services
{
    public interface IInputService
    {
        string ReadLine();
    }
}
