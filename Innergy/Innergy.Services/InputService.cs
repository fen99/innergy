﻿using System;

namespace Innergy.Services
{
    public class InputService : IInputService
    {
        public string ReadLine() => Console.ReadLine();
    }
}
