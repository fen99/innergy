﻿using System;
using System.Collections.Generic;

namespace Innergy.Services
{
    public class StandardInputServcie : IStandardInputServcie
    {
       private readonly IInputService inputService;
        public StandardInputServcie(IInputService inputService)
        {
            this.inputService = inputService;
        }

        public IEnumerable<string> ReadText(bool skipLines = false, Func<string,bool> skipCondition = null)
        {
            string line;
            var lines = new List<string>();
            do
            {
                line = inputService.ReadLine();
                if (skipLines && skipCondition != null && skipCondition(line))
                    continue;

                lines.Add(line);
            } while (!string.IsNullOrWhiteSpace(line));
            return lines;
        }
    }
}
