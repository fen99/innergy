﻿using System;
using System.Collections.Generic;

namespace Innergy.Services
{
    public interface IStandardInputServcie
    {
        IEnumerable<string> ReadText(bool skipLines = false, Func<string,bool> skipCondition = null);
    }
}
