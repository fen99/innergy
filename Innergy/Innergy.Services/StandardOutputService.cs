﻿using System;

namespace Innergy.Services
{
    public class StandardOutputService: IStandardOutputService
    {
        public void WriteLine() => Console.WriteLine();
        public void WriteLine(string text) => Console.WriteLine(text);
    }
}
