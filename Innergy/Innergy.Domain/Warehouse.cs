﻿using System.Collections.Generic;

namespace Innergy.Domain
{
    public class Warehouse
    {
        public string Name { get; set; }
        public IList<MaterialInfo> MaterialsInfos { get; set; }
        public int TotalMaterials { get; set; }
    }
}
