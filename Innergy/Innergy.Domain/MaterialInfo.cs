﻿namespace Innergy.Domain
{
    public class MaterialInfo
    {
        public string Name { get; set; }
        public string Id { get; set; }
        public int Quantity { get; set; }
    }
}
